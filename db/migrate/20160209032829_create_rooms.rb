class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :ownerName
      t.string :email
      t.integer :phoneNumber
      t.string :listingName
      t.text :summary
      t.string :property
      t.integer :room
      t.integer :price
      t.string :city
      t.string :place
      t.string :water
      t.string :amenities
      t.boolean :is_published

      t.timestamps null: false
    end
  end
end
