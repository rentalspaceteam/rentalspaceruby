Rails.application.routes.draw do
  devise_for :users
  resources :rooms
  # devise_for :users
  root 'rooms#index'
  get '/listing/space/', to: 'rooms#new', as: 'listing'
end
