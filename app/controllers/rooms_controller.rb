class RoomsController < ApplicationController
    def index
        @rooms = Room.all
    end

    def new
      @room = Room.new
    end

    def create
      @room = Room.new(listing_params)
      respond_to do |format|
        if @room.save
          format.html { redirect_to @room, notice: 'Room was successfully Listed.' }
          format.json { render json: @room }
        else
          format.html {render :new}
          format.html {render json:@room.errors}
        end
      end
    end

    private

    def listing_params
      params.require(:room).permit(:ownerName, :email, :phoneNumber, :listingName, :summary, :property, :room, :price, :city, :place, :water, :amenities, :is_published)
    end
end
