import React from 'react';
import ReactDOM from 'react-dom';
import Homepage from 'homepage/Homepage';
import Listing from 'listing/Listing';
// var Listing = require('listing/components/listing');

window.app = {

    showHomePage: function(id,data){
        ReactDOM.render(
          <Homepage />, document.getElementById(id,data)
        );
    },

    showListingSpaceForm:function(id,data){
      ReactDOM.render(
          <Listing />, document.getElementById(id,data)
        );
    }
}