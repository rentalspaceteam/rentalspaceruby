import React from 'react';
import Navbar from './components/Navbar';
import Body from './components/Body';
// import Footer from './components/Footer';
// import { Router, Route, IndexRoute, hashHistory } from 'react-router';
class Homepage extends React.Component {
  render() {
    return (
            <div className="layer">
              <Navbar />
              <Body />
            </div>
      );
  }
}
 
export default Homepage;