import React from 'react';
import Dropzone from 'react-dropzone';
import getRadioOrCheckboxValue from './radioboxvalue';
import ReactDOM from 'react-dom';
import assign from 'object-assign';

var fieldValues = {
  ownerName:'tushant',
  email:'programmertushant@gmail.com',
  phoneNumber:'9842333833',
  listingName  : 'listing',
  summary  : 'summary',
  property : null,
  room     : null,
  price    : 1000,
  city     : 'city',
  place    : 'place',
  water:null,
  amenities:[],
  image:[]
  
}


class AddRent extends React.Component{
 constructor(props,context) {
        super(props,context);
        this.state = {
            step: 1
        };
    }

  saveValues(field_value) {
    return function() {
      fieldValues = Object.assign({}, fieldValues, field_value)
    }()
    console.log('fieldValues are', fieldValues);
  }


  nextStep(step) {
    var step = this.state.step;
    var newStep = step+1;
    this.setState({step:newStep});
  }

  previousStep(step) {
    var step = this.state.step;
    var newStep = step-1
    this.setState({
      step : newStep
    });
  }


  showStep() {
  switch (this.state.step) {
    case 1:
      return <RenderPersonalInfo fieldValues={fieldValues}
                            nextStep={this.nextStep.bind(this)}
                            previousStep={this.previousStep.bind(this)}
                            saveValues={this.saveValues.bind(this)} />
    case 2:
      return <RenderBasic fieldValues={fieldValues}
                            nextStep={this.nextStep.bind(this)}
                            previousStep={this.previousStep.bind(this)}
                            saveValues={this.saveValues.bind(this)} />
    case 3:
      return <RenderDescription fieldValues={fieldValues}
                           nextStep={this.nextStep.bind(this)}
                           previousStep={this.previousStep.bind(this)}
                           saveValues={this.saveValues.bind(this)} />
    case 4:
      return <RenderLocation fieldValues={fieldValues}
                           nextStep={this.nextStep.bind(this)}
                           previousStep={this.previousStep.bind(this)}
                           saveValues={this.saveValues.bind(this)} />
    case 5:
      return <RenderAmenities fieldValues={fieldValues}
                           nextStep={this.nextStep.bind(this)}
                           previousStep={this.previousStep.bind(this)}
                           saveValues={this.saveValues.bind(this)} />

    case 6:
      return <RenderPhotos fieldValues={fieldValues}
                           nextStep={this.nextStep.bind(this)}
                           previousStep={this.previousStep.bind(this)} />
  }
}

  render() {
    var style = {
      width : (this.state.step / 6 * 100) + '%'
    }

    return (
      <main>
        <span className="progress-step">Step {this.state.step}</span>
        <progress className="progress" style={style}></progress>
        {this.showStep()}
      </main>
    )
  }
};

class RenderPersonalInfo extends React.Component{
  render(){
    return(
            <div>
              <h3>Personal Information</h3>
              <p className="subtitle">Provide your authentic information so rent seekers can contact you</p>
              <hr/>
              <div className="col-md-4">
                <label htmlFor='name'>Owner Name</label>
                <input ref="name" defaultValue={this.props.fieldValues.ownerName} type="textbox" className="form-control" id="name" placeholder="Owner name" />
              </div>
              <div className="col-md-4">
                <label htmlFor="email">Email</label>
                <input ref="email" defaultValue={this.props.fieldValues.email} type="email" className="form-control" id="email" placeholder="email" />
              </div>
              <div className="col-md-4">
                <label htmlFor="phoneNumber">Phone Number</label>
                <input ref="phone" defaultValue={this.props.fieldValues.phoneNumber} type="textbox" className="form-control" id="phoneNumber" placeholder="phone number" />
              </div>
              <hr/>
                <div className="row continueBtn text-right">
                    <button className="btn how-it-works" ref="personalInfo" onClick={this.nextStep.bind(this)}>Continue</button>
                </div>
            </div>
      );
  }
  nextStep(step){
     var data = {
          ownerName  : this.refs.name.value,
          email : this.refs.email.value,
          phoneNumber: this.refs.phone.value,
        }
        console.log(data.ownerName);
        if ((data.ownerName)&&(data.email)&&(data.phoneNumber)) {
          this.props.saveValues(data);
          this.props.nextStep();
        }
        else{
          alert('please enter the name, email and phone number');
        }
  }
};


class RenderBasic extends React.Component{
  render(){
    return(
            <div>
                <h3>Help Rent seekers find the right fit</h3>
                <p className="subtitle">People searching on Rental Space can filter by listing basics to find a space that matches their needs.</p>
                <hr/>
                <div className='col-md-4 basicForm'>
                    <label htmlFor="price">Number of Rooms</label>
                    <select className = "form-control" defaultValue={this.props.fieldValues.room} name="Rooms" ref="rooms">
                        <option value="1">1 room</option>
                        <option value="2">2 rooms</option>
                        <option value="3">3 rooms</option>
                        <option value="4">4 rooms</option>
                        <option value="5">5 rooms</option>
                        <option value="6">6 rooms</option>
                        <option value="7">7 rooms</option>
                        <option value="8">8 rooms</option>
                        <option value="9">8+ rooms</option>
                    </select>
                </div>
                <div className="col-md-4 basicForm">
                    <label htmlFor="price">Property Type</label>
                    <select className="form-control" defaultValue={this.props.fieldValues.property} name="Property Type" ref="property">
                        <option value="appartment">Appartment</option>
                        <option value="house">House</option>
                        <option value="shop">Shop</option>
                        <option value="bunglow">Bunglow</option>
                        <option value="hostel">Hostel</option>
                    </select>
                </div>
                <div className="col-md-4 basicForm">
                    <label htmlFor="price">Price</label>
                    <input type="textbox" ref="price" defaultValue={this.props.fieldValues.price} className="form-control" id="price" placeholder="Enter Price" required />
                </div>
                <hr/>
                <div className="row continueBtn text-right">
                    <button className="btn how-it-works pull-left" onClick={this.props.previousStep.bind(this)}>Back</button>
                    <button className="btn how-it-works" ref="basic" onClick={this.nextStep.bind(this)}>Continue</button>
                </div>
            </div>
          );
  }
  nextStep(step){
    var priceValue = this.refs.price.value;
    var propertyValue = this.refs.property;
    {/* console.log('property value is', propertyValue); */}
    var propertySelectedValue = propertyValue.options[propertyValue.selectedIndex].value; 
    {/* console.log('selected property value is', propertySelectedValue); */}
    var roomValue = this.refs.rooms;
    var roomSelectedValue = roomValue.options[roomValue.selectedIndex].value;
    var data = {
      property  : propertySelectedValue,
      room     : roomSelectedValue,
      price     : this.refs.price.value,
    }
    if(data.price){
      this.props.saveValues(data);
      this.props.nextStep();
    }
    else{
      alert('You are requested you to enter the price');
    }
  }
};

class RenderDescription extends React.Component{
  render(){
    var marginValue = {marginTop:'1.5em'};
    return(
            <div>
                <h3>Tell Rent Seekers about your space</h3>
                <hr/>
                <div className="col-sm-12">
                    <label htmlFor="listingName">Listing Name</label>
                    <input ref="name" type="textbox" defaultValue={this.props.fieldValues.listingName} className="form-control" id="listingName" placeholder="Be clear" />
                </div>
                <div className="col-sm-12" style={marginValue}>
                    <label htmlFor="summary">Summary</label>
                    <textarea ref="summary" defaultValue={this.props.fieldValues.summary} className="form-control" id="summary" rows="3"></textarea>
                </div>
               <div className="row continueBtn">
                    <button className="btn how-it-works pull-left" onClick={this.props.previousStep.bind(this)}>Back</button>
                    <button className="btn how-it-works pull-right" onClick={this.nextStep.bind(this)}>Continue</button>
                </div>
            </div>
          );
  }
  nextStep(step){
    var data = {
      listingName  :this.refs.name.value,
      summary :this.refs.summary.value,
    }

    if ((data.listingName)&&(data.summary)){
       this.props.saveValues(data);
       this.props.nextStep();
    }
    else{
      alert('please enter the listing name and summary');
    }
   
  }
};


class RenderLocation extends React.Component{
    render(){
        return(
            <div>
                <h3>Help guests find your place</h3>
                <p className="subtitle">will use this information to find a place that’s in the right spot.</p>
                <hr/>
                <div className="col-md-6">
                    <label htmlFor="city">City</label>
                    <input ref="city" defaultValue={this.props.fieldValues.city} type="textbox" className="form-control" id="city" placeholder="Biratnagar" />
                </div>
                <div className="col-md-6">
                    <label htmlFor="placeName">Name of Place</label>
                    <input ref="place" defaultValue={this.props.fieldValues.place} type="textbox" className="form-control" id="placeName" placeholder="Ganesh Chowk" />
                </div><hr/>
                <div className="row continueBtn">
                    <button className="btn how-it-works pull-left" onClick={this.props.previousStep.bind(this)}>Back</button>
                    <button className="btn how-it-works pull-right" onClick={this.nextStep.bind(this)}>Continue</button>
                </div>
            </div>
            );
    }

     nextStep(step){
        var data = {
          city  :this.refs.city.value,
          place :this.refs.place.value,
        }
        if ((data.city)&&(data.place)) {
          this.props.saveValues(data);
          this.props.nextStep();
        }
        else{
          alert('please enter the name of city and place');
        }
        
    }
};


class RenderAmenities extends React.Component{
  renderOptions(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      if (type == 'checkbox') return this.props.fieldValues[name].indexOf(value) >= 0

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  }

  render() {
    return (
      <div>
        <h2>Facilities</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">Water Facility</span>
            {['yes', 'No', 'Takes Charge'].map(this.renderOptions.bind(this, 'radio', 'water'))}
          </li>
          <li className="checkbox">
            <span className="label">Favorite Colors</span>
            {['Kitchen', 'Cable', 'Attached Bathroom', 'Internet'].map(this.renderOptions.bind(this, 'checkbox', 'amenities'))}
          </li>
           <div className="row continueBtn">
              <button className="btn how-it-works pull-left" onClick={this.props.previousStep.bind(this)}>Back</button>
              <button className="btn how-it-works pull-right" onClick={this.nextStep.bind(this)}>Continue</button>
            </div>
        </ul>
      </div>
    )
  }

  nextStep(step) {
    // Get values via querySelector
    var water    = document.querySelector('input[name="water"]:checked')
    var amenities = document.querySelectorAll('input[name="amenities"]')

    var data = {
      water    : getRadioOrCheckboxValue(water),
      amenities : getRadioOrCheckboxValue(amenities)
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
};



const style = {
    borderWidth: 2,
    borderColor: 'black',
    borderStyle: 'dashed',
    borderRadius: 4,
    margin: 30,
    padding: 30,
    width: 200,
    transition: 'all 0.5s'
};

const activeStyle = {
    borderStyle: 'solid',
    backgroundColor: '#eee',
    borderRadius: 8
};


 class RenderPhotos extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            files: []
        };
    }

    onDrop(files) {
      console.log('Received files: ', files);
      this.setState({
          files: files
      });
           
           function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        var csrftoken = getCookie('csrftoken');
        function csrfSafeMethod(method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                }
            });
            var image = [];
            image = new FormData(files);
           // console.log('formdata image',image);
           //  var multiple_image = files;
           //  console.log('multiple_image',multiple_image);
            $.each(files,function(i,file){
              image.append('image',file);
            });
       $.ajax({
        url:"/upload/image/",
        data:image,
        contentType:false,
        processData:false,
        type:'POST',
        mimeType: "multipart/form-data",
        success: function(data) {
          console.log('success');
        }
       });
    }

    showFiles() {
        const { files } = this.state;
        console.log('files',files);

        if (!files.length) {
            return null;
        }

        return (
            <div>
                <h3>Dropped files: </h3>
                <ul className="gallery">
                    {
                        files.map((file, idx) => {
                            return (
                                <li className="col-md-3" key={idx}>
                                    <img src={file.preview} width={200}/>
                                    <div>{file.name}</div>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }

    render() {
      return (
           <div>
                <h3>Photos can bring your space to life</h3>
                <p>Add photos of spaces to give more insight of your space </p>
                <hr/>
                <div className="col-md-12">
                <form method="POST" encType="multipart/form-data">
                <input type="hidden" name="csrfmiddlewaretoken" value="{{ csrf_token }}"/>
                  <Dropzone onDrop={this.onDrop.bind(this)}
                          style={style}
                          activeStyle={activeStyle}>
                    Try dropping some files here, or click to select files to upload.
                </Dropzone>
              </form>
                {this.showFiles()}
              </div>
              <div className="row continueBtn text-right">
                    <button className="btn how-it-works pull-left" onClick={this.props.previousStep.bind(this)}>Back</button>
                    <button className="btn how-it-works" onClick={this.nextStep.bind(this)}>Submit</button>
               </div>
            </div>

      );
    }

       nextStep(step){
                  var sendData={'ownerName':this.props.fieldValues.ownerName,
                        'email':this.props.fieldValues.email,
                        'phoneNumber':this.props.fieldValues.phoneNumber,
                        'listingName':this.props.fieldValues.listingName,
                        'summary':this.props.fieldValues.summary,
                        'property':this.props.fieldValues.property,
                        'room':this.props.fieldValues.room,
                        'price':this.props.fieldValues.price,
                        'city':this.props.fieldValues.city,
                        'place':this.props.fieldValues.place,
                        'water':this.props.fieldValues.water,
                        'amenities':this.props.fieldValues.amenities.join(', ')
                      }
                      console.log(sendData.email);
                  $.ajax({
                  url:"/add/space/",
                  data:sendData,
                  type:'POST',
                  success: function(data) {
                    console.log('success');
                  }
                 });
       }
  
    
}

export default AddRent;