import React from 'react';

export default class Body extends React.Component {
  render() {
    const headingPadding = {padding:50};
    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <div className="heading" style={headingPadding}>
                        <h2 className="text-center">Listing Space</h2>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}